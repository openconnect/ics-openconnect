OpenConnect for Android is an SSL VPN client with support for multiple protocols:

* Cisco AnyConnect or OpenConnect
* Juniper Network Connect
* Palo Alto Networks GlobalProtect
* Pulse Connect Secure
* F5 BIG-IP SSL VPN
* Fortinet SSL VPN
* Array SSL VPN

Features:

* One-click connection (batch mode)
* Supports RSA SecurID and TOTP software tokens
* Keepalive feature to prevent unnecessary disconnections
* Compatible with ARMv7, x86, and MIPS devices
* No root required
* Based on the popular OpenConnect Linux package

Requirements:

* Android 6.0 or higher
* An account on a suitable VPN server

This product includes software developed by the OpenSSL Project for use in the
OpenSSL Toolkit (https://www.openssl.org/).

This product includes cryptographic software written by Eric Young
(eay@cryptsoft.com).
